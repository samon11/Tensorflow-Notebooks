# "Deep Learning with Tensorflow" outlined through Jupyter notebooks
Learn Tensorflow through a series of python3 Jupyter notebooks following through the book [Deep Learning with Tensorflow 2nd Edition](https://www.packtpub.com/big-data-and-business-intelligence/deep-learning-tensorflow-second-edition) by Giancarlo Zaccone and Md. Rezaul Karim.

I hope for this repo to become a good learning __and__ reference resource not only for myself but for the beginner ML community as well. 

These notebooks were created using Tensorflow version __1.12__.
